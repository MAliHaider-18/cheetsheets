# Installing any python version

    pyenv install 3.9.12

# Making global python version

    pyenv global 3.9.12

# checking python versions

    pyenv versions

# To trigger specific python version

    pyenv shell 3.6.8


