# For Unable to 'apt update' on Linux

## Step 1:
    
    $ sudo cp  /etc/apt/sources.list /etc/apt/sources.list.bak
    $ sudo sed -i -re 's/([a-z]{2}\.)?archive.ubuntu.com|security.ubuntu.com/old-releases.ubuntu.com/g' /etc/apt/sources.list

## Step 2:

    $ sudo apt-get update && sudo apt-get dist-upgrade 
