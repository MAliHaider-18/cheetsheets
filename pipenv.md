# Use specific version fro virtual envirnment.

    pipenv --python 3.9.8

# Create or Activate virtual envirnment for new project.

    pipenv shell

# Create virtual envirnment for existing project with pipfile.

    pipenv install

# Install dependency in virtualenv.

    pipenv install <package-name>

# Making requirements file.

    pipenv requirements > requirements.txt 


