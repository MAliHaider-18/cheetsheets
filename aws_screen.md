# To check all available screens running

    $ screen -list

# To start new Screen.

    $ screen -S project_screen

# To resume existing Screen.

    $ screen -r project_screen

# To remove the existing Screen

    $ screen -XS project_screen quit

