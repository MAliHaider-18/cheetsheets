# Create a Service file.
    
    sudo nano /etc/systemd/system/application.service

    """

        [Unit]
        Description = Test Project API
        After=network.target

        [Service]
        User=ubuntu
        WorkingDirectory=/home/ubuntu/project_folder/
        ExecStart=/home/ubuntu/.local/bin/pipenv run python3 api.py

        [Install]
        WantedBy=multi-user.target


    """

# Configuring systemd service.
    
    sudo systemctl daemon-reload
    sudo systemctl enable application.service
    sudo systemctl start application.service

# Additional Commands

- ## Start a service.

        sudo systemctl start application.service
                        OR
        sudo systemctl start application

- ## Stop a service

        sudo systemctl stop application.service

- ## Restart a service
    
        sudo systemctl restart application.service

- ## Reload a service
    
        sudo systemctl reload application.service

- ## Enable a service

        sudo systemctl enable application.service

- ## Disable a service

        sudo systemctl disable application.service

- ## Get the status log of a service

        systemctl status application.service
    



